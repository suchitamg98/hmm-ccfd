import warnings
import numpy as np
from hmm_learn.hmm import GaussianHMM
from multiprocessing import Pool
import config as cfg
import pandas as pd
from preprocessing import preprocessing as prep
import cPickle
from multiprocessing import Pool
from functools import partial


# @profile
def train_HMM_and_calculate_anomaly_score(grpCH, df_test_rf, df_val_rf, df_rf):
    pd.DataFrame.very_deep_copy = cfg.very_deep_copy
    iterator=[['TX_AMOUNT', 5, True],['tdelta',5, True]]

    for thing in iterator:
        train_HMM_using_compromised_sequencies(grpCH, thing)
        train_HMM_using_genuine_sequencies(grpCH, thing)

    cfg.log('anomaly score for train rf\n')
#    df_rf = pd.concat([create_rf_train(cfg.genuinetrainrf, grpCH),
#                       create_rf_train(cfg.fraudulenttrainrf, grpCH)],
#                      ignore_index=True)
    df_rf = anomaly_score_on_testing_days(df_rf, grpCH)

    cfg.log(" : calculating new test set including history based feature ...\n")

    df_test_rf= anomaly_score_on_testing_days(df_test_rf, grpCH)
    df_val_rf = anomaly_score_on_testing_days(df_val_rf, grpCH)

    with open(cfg.PATHTOXP + 'rf_dbs.pkl', 'w') as saver:
        cPickle.dump([df_rf, df_test_rf, df_val_rf], saver)

    return df_rf, df_test_rf, df_val_rf


# @profile
def train_TM_HMM_and_calculate_anomaly_score(grpTM, df_rf, df_test_rf,
                                             df_val_rf):
    iterator = [['TX_AMOUNT', 5, False],['tdelta', 5, False]]

    for thing in iterator:
        c=0
        train_HMM_using_compromised_sequencies(grpTM, thing)
        train_HMM_using_genuine_sequencies(grpTM, thing)

    cfg.log(" : calculating new test set including history based feature ...\n")
    df_test_rf = term_anomaly_score_on_testing_days(df_test_rf, grpTM)
    df_val_rf = term_anomaly_score_on_testing_days(df_val_rf, grpTM)

    cfg.log('TM_anomaly_score for train rf\n')
    df_rf = term_anomaly_score_on_testing_days(df_rf, grpTM)

    with open(cfg.PATHTOXP + 'rf_dbs.pkl', 'w') as saver:
        cPickle.dump([df_rf, df_test_rf, df_val_rf], saver)
    return df_rf, df_test_rf, df_val_rf

def train_HMM_using_compromised_sequencies(grpCH, iter):
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    n_components=iter[1]
    feature=iter[0]
    cardholder=iter[2]
    if cardholder:
        with open(cfg.fraudulenttrainrf, 'r') as filenames:
            ls_file = filenames.readlines()
            save=cfg.fraudulent_hmm
    else:
        with open(cfg.compromisedtermhmm, 'r') as filenames:
            ls_file = filenames.readlines()
            save=cfg.hmm_term_compromised
    db = []
    lengths = []
    for filename in ls_file:
       seq = grpCH.get_group(filename[:-1])
       ls_date = seq['TX_DATETIME'].values
       ls_symbols = seq[feature].values
       ls_fraud = seq['TX_FRAUD'].values
       windows, windows_lengths = prep.get_inputs_for_compromised_hmm(
           ls_date, ls_symbols, ls_fraud)

       if windows_lengths>0:
           db.extend(windows)
           lengths.extend(windows_lengths)

    model = GaussianHMM(n_components=n_components, covariance_type='full', n_iter=cfg.n_iter, verbose=True)

    cfg.log('\tfit compromised HMM\n')
    cfg.log('n_seq = ' + str(len(lengths)) + '\n')
    db=np.hstack(db)
    model.fit(np.atleast_2d(db).T, lengths)

    if cardholder:
        with open(save+feature+str(n_components), 'w') as saver:
            cPickle.dump(model, saver)
    else:
        with open(save+feature+str(n_components), 'w') as saver:
            cPickle.dump(model, saver)


def train_HMM_using_genuine_sequencies(grpCH, iter):
    """
    Train the Multinomial Hidden Markov Model using genuine sequencies
    """
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    n_components=iter[1]
    feature=iter[0]
    cardholder=iter[2]
    if cardholder:
        with open(cfg.genuinetrainhmm, 'r') as filenames:
            ls_file = filenames.readlines()  # list of sequencies filenames to train HMM
    else:
        with open(cfg.genuinetermhmm, 'r') as filenames:
            ls_file = filenames.readlines()

    db = []
    lengths = []
    cfg.log(' : Training genuine HMM...\n')
    for filename in ls_file:
       seq = grpCH.get_group(filename[:-1])
       ls_date = seq['TX_DATETIME'].values  # [1:]
       ls_symbols = seq[feature].values #[1:]
       windows, windows_lengths = prep.get_inputs_for_hmm_training(
           np.array(ls_date),
           ls_symbols)
       if windows_lengths>0:
           db.extend(windows)
           lengths.extend(windows_lengths)
    model = GaussianHMM(n_components=n_components, covariance_type='full', n_iter=cfg.n_iter, verbose=True)

    cfg.log('\tfit genuine HMM\n')
    cfg.log('n_seq = ' + str(len(lengths)) + '\n')
    db=np.hstack(db)
    # print db
    # print np.atleast_2d(db).T
    # print lengths
    model.fit(np.atleast_2d(db).T, lengths)

    # print model.transmat_
    # print model.means_
    if cardholder:
        with open(cfg.genuine_hmm+feature+str(n_components), 'w') as saver:
            cPickle.dump(model, saver)
    else:
        with open(cfg.hmm_term_genuine+feature+str(n_components), 'w') as saver:
            cPickle.dump(model, saver)


def create_rf_train(ls_seq, grpCH):
    with open(ls_seq, 'r') as seq_list:
        ls_seq = seq_list.readlines()
    ls_df = []
    for id_seq in ls_seq:
        seq=grpCH.get_group(int(id_seq[:-1])).very_deep_copy()
        ls_df.append(seq[seq['TX_DATETIME']< np.datetime64(cfg.t_val)])
    return pd.concat(ls_df, ignore_index=True)


def calculate_aggregation_CH(df_test_rf, seq, ls_date, ls_amount, ls_country, ls_mcc):
    for index, row in seq.iterrows():
        t_row = np.datetime64(row['TX_DATETIME'])
        t_prev = t_row - 24 * 3600 * 1000000
        index1 = ls_date.searchsorted(
            t_row)
        index0 = ls_date.searchsorted(
            t_prev)
        country_row = row['TERM_COUNTRY']
        mcc_row = row['TERM_MCC']
        agg1 = 0
        agg2 = 0
        agg3 = 0
        agg4 = 0
        agg5 = 0
        agg6 = 0

        for t, amount, country, mcc in zip(ls_date[index0[0]:index1[0]], ls_amount[index0[0]:index1[0]],
                                      ls_country[index0[0]:index1[0]], ls_mcc[index0[0]:index1[0]]):
            agg1 += 1
            agg2 += amount
            if country == country_row:
                agg3 += 1
                agg4 += amount
            if mcc == mcc_row:
                agg5 += 1
                agg6 += amount
        df_test_rf.set_value(index, 'aggCH1', agg1)
        df_test_rf.set_value(index, 'aggCH2', agg2)
        df_test_rf.set_value(index, 'aggCH3', agg3)
        df_test_rf.set_value(index, 'aggCH4', agg4)
        df_test_rf.set_value(index, 'aggCH5', agg5)
        df_test_rf.set_value(index, 'aggCH6', agg6)
    return


def anomaly_score_on_testing_days(df_test_rf, grpCH):
    """
    Scan the transactions from the test set, get the past transactions from these card-holder, do the vector quantization
    and calculate anomaly score
    """
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    with open(cfg.genuine_hmm+'TX_AMOUNT5', 'r') as loader:
        model_genuine_amount5 = cPickle.load(loader)
    with open(cfg.fraudulent_hmm+'TX_AMOUNT5', 'r') as loader:
        model_fraud_amount5 = cPickle.load(loader)
    with open(cfg.genuine_hmm+'tdelta5', 'r') as loader:
        model_genuine_tdelta5 = cPickle.load(loader)
    with open(cfg.fraudulent_hmm+'tdelta5', 'r') as loader:
        model_fraud_tdelta5 = cPickle.load(loader)

    print 'anomaly score on testing days'
    index_to_drop = []
    window_size=3
    grp = df_test_rf.groupby('newPanID')
    for newpan, seq_to_fill in grp:
        seq_ref = grpCH.get_group(newpan)
        ls_date = seq_ref['TX_DATETIME']
        tdeltas=seq_ref['tdelta']
        amounts = seq_ref['TX_AMOUNT']
        ls_country = seq_ref['TERM_COUNTRY']
        ls_mcc = seq_ref['TERM_MCC']
        calculate_aggregation_CH(df_test_rf, seq_to_fill, ls_date,
                                      amounts, ls_country,ls_mcc)
        for index, row in seq_to_fill.iterrows():
            windows_tdeltas = prep.take_past_symbols_for_test_anomaly_score(
                row['TX_DATETIME'],
                ls_date, tdeltas, window_size)
            windows_amounts = prep.take_past_symbols_for_test_anomaly_score(row['TX_DATETIME'],ls_date, amounts, window_size)
            if len(windows_tdeltas) == window_size and len(windows_amounts) == window_size:
                iterator=[[model_fraud_amount5, windows_amounts, window_size, '_5_amount_CH_fraud', index],
                          [model_genuine_amount5, windows_amounts, window_size, '_5_amount_CH_genuine', index],
                          [model_fraud_tdelta5, windows_tdeltas, window_size, '_5_tdelta_CH_fraud', index],
                          [model_genuine_tdelta5, windows_tdeltas, window_size, '_5_tdelta_CH_genuine', index]]

                for thing in iterator:
                    setvalue(df_test_rf, thing)
            else:
                index_to_drop.append(index)
    df_test_rf.drop(index_to_drop, inplace=True)
    return df_test_rf

def setvalue(df_test_rf, iter):
    df_test_rf.set_value(iter[4], str(iter[2])+iter[3], iter[0].score(np.atleast_2d(iter[1].values).T))


def term_anomaly_score_on_testing_days(df_test_rf, grpTM):
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    with open(cfg.hmm_term_genuine+'TX_AMOUNT5', 'r') as loader:
        model_genuine_amount5 = cPickle.load(loader)
    with open(cfg.hmm_term_compromised+'TX_AMOUNT5', 'r') as loader:
        model_fraud_amount5 = cPickle.load(loader)
    with open(cfg.hmm_term_genuine+'tdelta5', 'r') as loader:
        model_genuine_tdelta5 = cPickle.load(loader)
    with open(cfg.hmm_term_compromised+'tdelta5', 'r') as loader:
        model_fraud_tdelta5 = cPickle.load(loader)

    index_to_drop = []
    window_size=3
    grp = df_test_rf.groupby('TERM_MIDUID')
    for TM, seq_to_fill in grp:
        seq_ref = grpTM.get_group(TM)
        ls_date = seq_ref['TX_DATETIME']
        tdeltas = seq_ref['tdelta']
        amounts = seq_ref['TX_AMOUNT']
        ls_entry_mode = seq_ref['TX_CARD_ENTRY_MODE']
        ls_card_type = seq_ref['CARD_TYPE']
        calculate_aggregation_TM(df_test_rf, seq_to_fill, ls_date, amounts, ls_card_type, ls_entry_mode)
        for index, row in seq_to_fill.iterrows():
            windows_tdeltas = prep.take_past_symbols_for_test_anomaly_score(
                row['TX_DATETIME'],
                ls_date, tdeltas, window_size)
            windows_amounts = prep.take_past_symbols_for_test_anomaly_score(
                row['TX_DATETIME'],ls_date, amounts, window_size)
            if len(windows_tdeltas) == window_size and len(windows_amounts) == window_size:
                iterator=[[model_fraud_amount5, windows_amounts, window_size, '_5_amount_TM_fraud', index],
                          [model_genuine_amount5, windows_amounts, window_size, '_5_amount_TM_genuine', index],
                          [model_fraud_tdelta5, windows_tdeltas, window_size, '_5_tdelta_TM_fraud', index],
                          [model_genuine_tdelta5, windows_tdeltas, window_size, '_5_tdelta_TM_genuine', index]]

                for thing in iterator:
                    setvalue(df_test_rf, thing)
            else:
                index_to_drop.append(index)
    df_test_rf.drop(index_to_drop, inplace=True)
    return df_test_rf

def calculate_aggregation_TM(df_test_rf, seq, ls_date, ls_amount, ls_card_type, ls_entry_mode):
    for index, row in seq.iterrows():
        t_row = np.datetime64(row['TX_DATETIME'])
        t_prev = t_row - 24 * 3600 * 1000000
        index1 = ls_date.searchsorted(
            t_row)
        index0 = ls_date.searchsorted(
            t_prev)
        card_type_row = row['CARD_TYPE']
        entry_mode_row = row['TX_CARD_ENTRY_MODE']
        agg1 = 0
        agg2 = 0
        agg3 = 0
        agg4 = 0
        agg5 = 0
        agg6 = 0
        for t, amount, card_type, entry_mode in zip(ls_date[index0[0]:index1[0]], ls_amount[index0[0]:index1[0]],
                                      ls_card_type[index0[0]:index1[0]], ls_entry_mode[index0[0]:index1[0]]):
            agg1 += 1
            agg2 += amount
            if card_type == card_type_row:
                agg3 += 1
                agg4 += amount
            if entry_mode == entry_mode_row:
                agg5 += 1
                agg6 += amount
        df_test_rf.set_value(index, 'aggTM1', agg1)
        df_test_rf.set_value(index, 'aggTM2', agg2)
        df_test_rf.set_value(index, 'aggTM3', agg3)
        df_test_rf.set_value(index, 'aggTM4', agg4)
        df_test_rf.set_value(index, 'aggTM5', agg5)
        df_test_rf.set_value(index, 'aggTM6', agg6)
    return

