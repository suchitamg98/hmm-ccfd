import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
import cPickle
import config as cfg
import os, sys


def create_encoders():
    pd.DataFrame.very_deep_copy = cfg.very_deep_copy
    ls_filename = os.listdir(cfg.raws)
    big_df = pd.DataFrame()
    ls_filename.sort()
    for filename in ls_filename:
        cfg.log(filename + '\n')
        df = pd.read_csv(cfg.raws + filename, usecols=cfg.use_cols.keys(),dtype=cfg.use_cols,
                         parse_dates=['TX_DATETIME'])
        df=df[df['CARD_TRAVEL_KEY']==False]

        big_df = big_df.append(df,
                               ignore_index=True)  # delete_non_used_columns(pd.read_csv(cfg.raws+filename)))
    print big_df.shape
    big_df = big_df.sort_values(['TX_DATETIME'])
    cfg.log(str(big_df.columns))
    for feature in big_df.columns:
       if feature not in cfg.use_cols.keys():
           big_df.drop([feature], axis=1, inplace=True)
    df['DT_wday'] = df['TX_DATETIME'].dt.weekday
    df['DT_hour'] = df['TX_DATETIME'].dt.hour
    big_df.fillna(-1, inplace=True)
    create_label_encoders(big_df)
    cfg.log(str(big_df.columns))
    return big_df


def create_label_encoders(df):
    """
    Create and store label-encoder object (sklearn) using informations on categorical features
    """
    cfg.log(' : initializing label encoder..\n')
    encoders = {}
    for feature in df.columns:
#        if features_type[feature] == 'categorical' or features_type[feature] == 'binary':
        if cfg.use_cols[feature]=='object':
            encoders[feature] = LabelEncoder()
            encoders[feature].fit(df[feature].astype(str))
#            encoders[feature].classes_ =   df[feature].unique()

    with open(cfg.encoders, 'w') as save:
        cPickle.dump(encoders, save)

features_type = {'TX_PROCESS': 'categorical', 'TERM_MCC': 'categorical',
		'TX_LOCAL_AMOUNT':'numerical',
                 'TERM_COUNTRY': 'categorical', 'TX_DATETIME': 'date',
                 'CARD_AUTHENTICATION': 'categorical',
                 'TX_CARD_ENTRY_MODE': 'categorical',
                 'TX_ECOM_IND': 'categorical', 'DT_wday': 'date',
                 'DT_hour': 'date',
                 'TX_AMOUNT': 'numerical', 'TX_ECOM': 'binary',
                 'TX_EMV': 'binary', 'TX_INTL': 'binary',
                 'TX_3D_SECURE': 'binary', 'newPanID': 'categorical',
                 'TERM_MIDUID': 'categorical',
                 'ZIP': 'categorical', 'CITY': 'categorical',
                 'INS_CODE': 'categorical', 'COUNTRY': 'categorical',
                 'PROVINCE_CODE': 'categorical', 'DISTRICT_CODE': 'categorical',
                 'CREDIT_LIMIT': 'numerical',
                 'CARD_EXPIRY': 'numerical', 'TX_LOCAL_CURRENCY': 'categorical',
                 'CARD_TYPE': 'categorical',
                 'BROKER': 'categorical', 'GENDER': 'categorical',
                 'AGE': 'numerical', 'CARD_BRAND': 'categorical',
                 'LANGUAGE': 'categorical', 'CARD_TRAVEL_KEY': 'binary',

                 'score_tdelta_CH_fraud':'added','score_tdelta_CH_genuine':'added',
                 'score_tdelta_TM_fraud':'added','score_tdelta_TM_genuine':'added',
                'score_amount_CH_fraud':'added','score_amount_CH_genuine':'added',
                 'score_amount_TM_fraud':'added','score_amount_TM_genuine':'added',
                 'score_amount-tdelta_CH_fraud': 'added', 'score_amount-tdelta_CH_genuine': 'added',
                 'score_amount-tdelta_TM_fraud': 'added', 'score_amount-tdelta_TM_genuine': 'added',
                 'TX_FRAUD': 'target', 'tdelta':'duration',
                 'aggCH1':'added','aggCH2':'added','aggCH3':'added','aggCH4':'added', 'aggCH5': 'added', 'aggCH6': 'added',
                 'aggTM1': 'added', 'aggTM2': 'added', 'aggTM3': 'added', 'aggTM4': 'added', 'aggTM5': 'added', 'aggTM6':'added',
                    '3_5_amount_CH_fraud':'added',
                    '3_5_amount_CH_genuine':'added',
                    '3_5_amount_TM_fraud':'added',
                    '3_5_amount_TM_genuine':'added',
                    '3_5_tdelta_CH_fraud':'added',
                    '3_5_tdelta_CH_genuine':'added',
                    '3_5_tdelta_TM_fraud':'added',
                '3_5_tdelta_TM_genuine':'added',

                'merchant_category_code':'categorical',
                'term_country':'categorical',
                "enc_activity_sector":'categorical',
                 "enc_term_continent":'categorical',
                "enc_tx_is_3dsecure":'categorical',
                "enc_card_holder_gender":'categorical',
                "enc_card_holder_broker":'categorical',
                "enc_card_type":'categorical',
                "enc_card_holder_continent":'categorical',
                "enc_tx_card_authentication":'categorical',
                "enc_tx_is_international":'categorical',
                "enc_tx_card_entry_mode":'categorical',
                "enc_ecom_ind":'categorical',
    "tx_amount":'numeric',
    "card_holder_age":'numeric'}

def preprocessing_from_pandas_to_sklearn(db, feature_list):
    db, y = feature_selection(db, feature_list)
    db = label_encoding_dataframe(db)
    return db, y


def feature_selection(db, feature_list):
    """
    Select features to use from the pandas dataframe (feature_list - feat_to_del)
    transform TX_DATETIME into hour of the day and day of the week

    :return: db with selected features, target variable
    """
    y = (db['TX_FRAUD'] == True).astype(int)
    db.drop(['TX_FRAUD'], axis=1, inplace=True)
    for feature in db.columns:
        if feature not in feature_list:
            db.drop([feature], axis=1, inplace=True)
    return db, y


def label_encoding_dataframe(db, types_to_encode=['binary','categorical']):
    with open(cfg.encoders, 'r') as loader:
        encoders = cPickle.load(loader)
    for feature in db.columns:
        if feature in cfg.use_cols.keys():
            if cfg.use_cols[feature]=='object':
                print feature
                db[feature] = encoders[feature].transform(db[feature].astype(str))
    return db




def get_inputs_for_compromised_hmm(ls_date, ls_symbol, ls_fraud):
    index = ls_date.searchsorted(np.datetime64(cfg.t_max))
    windows = []
    lengths = []
    for i in range(index - cfg.tx_in_past + 1):
        if ls_fraud[i:i + cfg.tx_in_past].sum() > 0:
            windows.extend(ls_symbol[i:i + cfg.tx_in_past])
            lengths.append(cfg.tx_in_past)
    return windows, lengths


def get_inputs_for_hmm_training(ls_date, ls_symbol):
    index = ls_date.searchsorted(np.datetime64(cfg.t_max))
    windows = []
    lengths = []
    for i in range(index - cfg.tx_in_past + 1):
        windows.extend(ls_symbol[i:i + cfg.tx_in_past])
        lengths.append(cfg.tx_in_past)
    return windows, lengths



def take_past_symbols_for_test_anomaly_score(dt_tx, ls_date, ls_symbols, window_size):
    index = ls_date.searchsorted(np.datetime64(dt_tx))
    if index[0] >= window_size:
        return ls_symbols[index[0] - window_size: index[0]]
    else:
        return []


def calculate_time_delta(bigdf):
    cfg.log('calculate_time_delta..\n')
    bigdf['tdelta'] = 0
    grpCH = bigdf.groupby('newPanID')
    for CH, seq in grpCH:
        ctx = 0
        for index, row in seq.iterrows():
            if ctx == 0:
                ind_init = index
                dt_past = row['TX_DATETIME']
            if ctx > 0:
                dt_now = row['TX_DATETIME']
                delta = dt_now - dt_past
                dt_past = dt_now
                bigdf.set_value(index, 'tdelta', delta.seconds)
            ctx += 1
    return bigdf
