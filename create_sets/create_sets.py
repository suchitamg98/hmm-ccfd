from datetime import datetime
import os, cPickle
import math
from random import shuffle
import config as cfg
import csv
import numpy as np


def create_sequencies_and_trainsets(grpCH):
    caracs=[]
    for CH, seq in grpCH:
        a=get_group_charac(CH, seq)
        caracs.append(a)
    shuffle(caracs)
    with open(cfg.PATHTOXP+'ls_caracs_CH.pkl','w') as save:
        cPickle.dump(caracs, save)
    with open(cfg.PATHTOXP+'ls_caracs_CH.pkl','r') as loa:
        caracs=cPickle.load(loa)
    select_sequencies_for_trainsets_bis(caracs)
    return grpCH


def create_term_sequencies(grpTM):
    genuine_term_HMM = open(cfg.genuinetermhmm, 'w')
    compromised_term_HMM = open(cfg.compromisedtermhmm, 'w')
    cfg.log('choose training sequencies for TM_HMM \n')
    ls_genuine_TM=[]
    for TM, seq in grpTM:
        seq_before = seq[seq['TX_DATETIME'] < np.datetime64(cfg.t_max)]
        if len(seq_before.index)>cfg.tx_in_past:
            if seq_before['TX_FRAUD'].sum() >= 1:
                compromised_term_HMM.write(TM+'\n')
            else:
                ls_genuine_TM.append(TM)
    shuffle(ls_genuine_TM)
    for i in range(cfg.nb_genuine_term_HMM):
        genuine_term_HMM.write(str(ls_genuine_TM[i])+'\n')
    genuine_term_HMM.close()
    compromised_term_HMM.close()
    return grpTM


def get_group_charac(CH, seq):
    seq_before = seq[: seq['TX_DATETIME'].searchsorted(np.datetime64(cfg.t_max))[0]]
    return [CH, len(seq), seq['TX_FRAUD'].sum(), len(seq_before), seq_before['TX_FRAUD'].sum()]


def select_sequencies_for_trainsets_bis(ls_info):
    """
    Read the cfg.seq_info file containing the characteristics of the sequencies
    Create files to list the selected sequencies that will be used in the trainsets
    """
    cfg.log(' : selecting files for trainsets ...\n')
    ls_info = ls_info[1:]  # delete header
    shuffle(ls_info)  # permet le choix aleatoire

    fraud_train_rf = open(cfg.fraudulenttrainrf, 'w')
    genuine_train_HMM = open(cfg.genuinetrainhmm, 'w')
    genuine_train_rf = open(cfg.genuinetrainrf, 'w')

    genuine_sequencies = 0
    compromised_sequencies = 0
    ls_genuine_newpans = []

    for info in ls_info:
        # info de la forme: [newpanID, nb_tx_total, nb_fraud_total, nb_tx_in_past, nb_fraud_in_past]
        if int(info[3]) >= cfg.tx_in_past:
            if int(info[4]) > 0:  # Fraude dans cette sequence dans le passe
                fraud_train_rf.write(str(info[0]) + '\n')
                compromised_sequencies += 1
            else:
                ls_genuine_newpans.append(info[0])
                genuine_sequencies += 1
    n_gen_rf = int(math.floor(cfg.genuine_over_fraud_ratio * compromised_sequencies))
    n_gen_hmm = int(math.floor(cfg.remaining_genuine_to_train_HMM * (
    genuine_sequencies - n_gen_rf)))  # Percentage of remaining sequencies
    nb_sequencies_selected = 0
    while nb_sequencies_selected < n_gen_hmm:
        genuine_train_HMM.write(str(ls_genuine_newpans[nb_sequencies_selected]) + '\n')
        nb_sequencies_selected += 1
    while nb_sequencies_selected < n_gen_rf + n_gen_hmm:
        genuine_train_rf.write(str(ls_genuine_newpans[nb_sequencies_selected]) + '\n')
        nb_sequencies_selected += 1

    fraud_train_rf.close()
    genuine_train_rf.close()
    genuine_train_HMM.close()


def get_DATETIME(tx):
    return datetime.strptime(tx[5], '%Y-%m-%d %H:%M:%S')


def is_fraudulent(tx):
    if tx[-3] == 'TRUE':
        return True
    else:
        return False
