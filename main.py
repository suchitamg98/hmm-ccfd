import pandas as pd
import config as cfg
import cPickle
import numpy as np
import init_directories
from preprocessing import preprocessing as prep
from create_sets import create_sets as create_sets
from anomaly_detection import HMM as HMM
from fraud_detection import random_forest as rf
import sys


def main():
    init_directories.main_function(initialisation=False)
    sys.stderr = open(cfg.error, 'w')
    cfg.log(' : STARTING EXPERIMENT\n')
    
    cfg.config_parameters(cfg.PATHTOXP)
    big_df = prep.create_encoders()

    
    big_df=prep.calculate_time_delta(big_df)

    big_df['aggCH1'] = 0.0 #sum TX
    big_df['aggCH2'] = 0.0 #sum amount
    big_df['aggCH3'] = 0.0 #sum TX in country
    big_df['aggCH4'] = 0.0 #sum amount in country

    big_df['aggTM1'] = 0.0 #sum TX
    big_df['aggTM2'] = 0.0 #sum amount
    big_df['aggTM3'] = 0.0 #sum TX in country
    big_df['aggTM4'] = 0.0 #sum amount in country
    print 'Create CH sequencies..'

    grpCH=big_df.groupby(['newPanID'])
    ind_to_del=[]
    for CH, seq in grpCH:
       ind_to_del.append(seq.index[0])
    del grpCH
    big_df.drop(ind_to_del, inplace=True)

    grpCH = big_df.groupby(['newPanID'])
    grpCH = create_sets.create_sequencies_and_trainsets(grpCH)

    df_past=big_df[big_df['TX_DATETIME']<np.datetime64(cfg.t_val)]
    df_rf=df_past[df_past['TX_FRAUD']==True]
    n_fraud_train=len(df_rf.index)
    cfg.log(str(n_fraud_train)+'\n')
    cfg.log(str(np.shape(df_past))+'\n')
    df_append=df_past[df_past['TX_FRAUD']==False].sample(n=50*n_fraud_train)
    cfg.log(str(np.shape(df_append))+'\n')
    df_rf=pd.concat([df_rf,df_append], ignore_index=True)
    cfg.log(str(np.shape(df_rf))+'\n')
    cfg.log(str(np.sum(df_rf['TX_FRAUD']))+'\n')

    df_test_rf = big_df[big_df['TX_DATETIME'] > np.datetime64(cfg.t_test)]
    df_val_rf=big_df[np.datetime64(cfg.t_val) < big_df['TX_DATETIME']]
    df_val_rf=df_val_rf[df_val_rf['TX_DATETIME'] < np.datetime64(cfg.t_max)]


    print 'HMM module for CH..'
    df_rf, df_test_rf, df_val_rf=HMM.train_HMM_and_calculate_anomaly_score(grpCH, df_test_rf, df_val_rf, df_rf)
    
    

    grpTM = big_df.groupby(['TERM_MIDUID'])
    grpTM = create_sets.create_term_sequencies(grpTM)

    print 'HMM module for TM..'
    df_rf, df_test_rf, df_val_rf= HMM.train_TM_HMM_and_calculate_anomaly_score(grpTM, df_rf, df_test_rf, df_val_rf)
    """
  # Uncomment the 2 next line and comment everything above if you want to only train random-forests on different feature sets
    with open(cfg.PATHTOXP+'rf_dbs.pkl','r') as loader:
        [df_rf, df_test_rf, df_val_rf]=cPickle.load(loader)
    """
    print 'Random Forest module..'
    rf.train_and_test_rf(df_rf, df_test_rf, df_val_rf)

    sys.stderr.close()
    sys.stderr = sys.__stderr__



if __name__ == '__main__':
    main()
