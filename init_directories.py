import os
import config as cfg
from shutil import rmtree

def main_function(initialisation=True):
    if initialisation==True:
        directory_names=[cfg.total_history, cfg.f2f, cfg.saveonehot, cfg.rawsvq, cfg.ecom, cfg.raws, cfg.PATHTODATA+'experiences/']
        for dirname in directory_names:
            if not os.path.isdir(dirname):
                os.makedirs(dirname)


    if initialisation==False:
        print 'Preparation of directories'
        xp_object_names=[cfg.fraudulenttrainrfvqscore, cfg.genuinetrainrfvqscore, cfg.results,
                         cfg.testfinal, cfg.python_objects, cfg.figures, cfg.testfinalbis, cfg.rawsvq]

        for dirname in xp_object_names:
            if not os.path.isdir(dirname):
                os.makedirs(dirname)

if __name__=='__main__':
    main_function()
